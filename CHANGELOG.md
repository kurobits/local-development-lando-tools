# CHANGELOG

## Version 8.0.4-alpha01

Had to update composer.json file due to errors at install. 
Composer project type is now a custom string "local-project".
This is useful when installing the project in our local environments.

Dependencies of this project include:

- "composer/installers": "^1.9"
- "oomphinc/composer-installers-extender": "^2.0"

These allow us to set the installation folders in the "extra" key in our local composer.json files:

```
"extra": {
    "installer-types": [
        "local-project"
    ],
    "installer-paths": {
        "local": [
            "type:local-project"
        ]
    }
}
```

This version also has functional SASS scripts. Colors + new command line parameters for the script `lando local`.


## Version 8.0.2

Creates lando.yml file with the following:

- Recipe: "drupal8", "pantheon", "platformsh"
- Lando webroot folder: "web", "."
- Overrides config for php version, database, webserver (apache/nginx)
- Sets up basic config for recipes
- Local tooling scripts (get)
- Use vendor drush
- Set composer php memory limit
- Install and use node-sass
- Downgrade composer to version 1.10.1

## Dev version

- adds SASS
- adds parameters to local get script: --environment, --component and --use-local-db