#!/bin/bash

DATABASE="db.sql"
declare -A CONFIG
declare -A EXTRA_CONFIG
declare -A PARAMS

FILE=$PWD/.local
if [[ -f "$FILE" ]]; then
  . $FILE
  # echo "-- variables from file"

  STAGE_FILE_PROXY_VERSION_FILE="stage_file_proxy-2.0.2.tar.gz"
  STAGE_FILE_PROXY_WGET_URL="https://ftp.drupal.org/files/projects/${STAGE_FILE_PROXY_VERSION_FILE}"
  WEB_MODULES_DEV="web/modules/dev"

else
  echo -e "\nLocal config file is not set. Please generate a \".local\" file at the root of the project based on the example in local/default.local\n\n"

  exit 0

fi
