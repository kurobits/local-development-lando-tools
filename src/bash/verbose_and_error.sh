#!/bin/bash

. "${PATH_LOCAL}/src/bash/colors.sh"

VERBOSE=0

# parse global parameters of CMD for Verbose options
array=( "$@" )
arraylength=${#array[@]}
for (( i=0; i<${arraylength}; i++ ));
do
  # environment
  if [[ "${array[$i]}" == -v ]];
  then
    VERBOSE=1
    echo -e "${GREEN}Set verbose mode to 1.${ENDCOLOR}"
  fi
  if [[ "${array[$i]}" == -vv ]];
  then
    VERBOSE=2
    echo -e "${GREEN}Set verbose mode to 2.${ENDCOLOR}"
  fi
  if [[ "${array[$i]}" == -vvv ]];
  then
    VERBOSE=3
    echo -e "${GREEN}Set verbose mode to 3.${ENDCOLOR}"
  fi
done


verbose_1_print() {
  if [[ $VERBOSE > 0 ]]; then
    echo -e "-> ${GREEN}${1}${ENDCOLOR}"
  fi
}

verbose_2_print() {
  if [[ $VERBOSE > 1 ]]; then
    echo -e "-> ${GREEN}${1}${ENDCOLOR}"
  fi
}

verbose_3_print() {
  if [[ $VERBOSE > 2 ]]; then
    echo -e "-> ${GREEN}${1}${ENDCOLOR}"
  fi
}

print_error() {
  echo -e "-> ${RED}${BOLD}${1}${ENDCOLOR}"
}