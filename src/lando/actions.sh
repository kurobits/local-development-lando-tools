#!/bin/bash

if [ -z ${LANDO_MOUNT+x} ];
then
  PATH_LOCAL="."
else
  # absolute path of the local folder within Lando
  PATH_LOCAL="/app/local"
fi


# include variables to use colors in messages
. "${PATH_LOCAL}/src/bash/colors.sh"

# set verbose requirements
. "${PATH_LOCAL}/src/bash/verbose_and_error.sh"



# Menu helper
help_menu() {
  echo -e "Pass different arguments to this script to handle your local environment:\n";
  echo -e "";
  echo -e "  get\t\t\tSyncs the files and database from our custom server:\n\t\t\t$ lando local get\n";
  echo -e "  \t\t\t${ITALIC}Or get more information about this CMD, calling:\n\t\t\t$ lando local get --help${ENDCOLOR}\n";
  echo -e "  --help\t\tPrints out this message\n";
  echo -e "  -v, -vv, -vvv\t\tEnables verbose mode\n";
}


########
# MENU #
########

while :; do
  case $1 in
    -h|-\?|--help)
      help_menu    # Display a usage synopsis.
      exit
      ;;
    get)
      # default values
      component="all"

      . "${PATH_LOCAL}/src/lando/get.sh"
      script_params=( "$@" )
      # set up parameters from arguments
      local_get_setup $script_params
      # call get function
      local_get $component;

      shift
      break
      ;;
    --) # End of all options.
      shift
      break
      ;;
    -?*)
      printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
      ;;
    *) # Default case: No more options, so break out of the loop.
      help_menu
      break
  esac

  shift
done
