#!/bin/bash

. "${PATH_LOCAL}/src/variables.sh"

rsync_server() {
  if [[ -z $STAGE_FILE_PROXY_ORIGIN_URL ]]; then
    verbose_1_print "Syncronizing files to local files folder"
    eval "rsync -avz --progress $REMOTE_FILES $PATH_TO_SITES_FOLDER/sites/default/files/";
  else
    verbose_1_print "Setting up Stage File Proxy"
    eval setup_stage_file_proxy
  fi
}

setup_stage_file_proxy() {

  EXIT_ERROR=false
  if [[ -z "$STAGE_FILE_PROXY_ORIGIN_URL" ]];then
    echo -e "${RED}ERROR: Unable to complete setup. Stage file proxy origin not set.${ENDCOLOR}"
    EXIT_ERROR=true
  fi
  if [[ -z "$STAGE_FILE_PROXY_VERSION_FILE" ]];then
    echo -e "${RED}ERROR: Unable to complete setup. Stage file proxy versioned file not set.${ENDCOLOR}"
    EXIT_ERROR=true
  fi
  if [[ -z "$STAGE_FILE_PROXY_WGET_URL" ]];then
    echo -e "${RED}ERROR: Unable to complete setup. Stage file proxy wget URL not set.${ENDCOLOR}"
    EXIT_ERROR=true
  fi

  if $EXIT_ERROR; then
    echo -e "${RED}EXIT ON ERROR${ENDCOLOR}"
    exit 1
  fi

  try_download_stage_file_proxy

  try_enable_stage_file_proxy

  drush cset stage_file_proxy.settings origin $STAGE_FILE_PROXY_ORIGIN_URL -y
  drush cr

  return
}

try_download_stage_file_proxy() {
  # Buscamos el modulo en el sistema, si no está lo bajamos a mano
  _stage_file_proxy_is_installed INSTALLED
  if [[ "$INSTALLED" == "false" ]];
  then
    # bajamos stage_file_proxy si no existe
    # lo bajamos por wget para no modificar el composer.json
    # luego lo ubicamos en una carpeta web/modules/dev para que composer no lo
    # trackee. Y lo agregamos al .gitignore para evitar que aparezca en git.

    verbose_2_print "Downloading Stage File Proxy module into ${WEB_MODULES_DEV}"

    wget $STAGE_FILE_PROXY_WGET_URL # obtenemos el tar del módulo
    tar -xf $STAGE_FILE_PROXY_VERSION_FILE # lo extraemos en /app

    # generamos el directorio web/modules/dev si no existe
    verbose_2_print "Creating dev directory for special modules: ${WEB_MODULES_DEV}"
    mkdir -p /app/${WEB_MODULES_DEV}/

    # ignoramos el directorio web/modules/dev en git para evitar commits
    # de este módulo
    IGNORED_IN_GIT=$(grep "${WEB_MODULES_DEV}" /app/.gitignore)
    if [[ "${IGNORED_IN_GIT}" = "" ]]; then
      verbose_2_print "Ignoring dev directory in git."
      echo -e "\n\n/${WEB_MODULES_DEV}\n${WEB_MODULES_DEV}\n" >> /app/.gitignore
    fi

    # movemos el directorio de stage_file_proxy dentro de web/modules/dev
    verbose_2_print "Adding Stage File Proxy to ${WEB_MODULES_DEV}."
    mv -f stage_file_proxy /app/${WEB_MODULES_DEV}/

    # borramos el tar de la descarga original de /app
    rm $STAGE_FILE_PROXY_VERSION_FILE*
  fi

  # validamos que el módulo esté en web/modules/dev
  _stage_file_proxy_is_installed INSTALLED
  if [[ "$INSTALLED" == "true" ]];  then
    verbose_1_print "Stage File Proxy is ready to be enabled."
  else
    print_error "Stage File Proxy is NOT ready to be enabled.\nEXIT ON ERROR"
    exit 1
  fi

  # todo validar texto en gitgnore
}

# Preguntamos si ya está prendido el módulo en la base,
# sino lo configuramos
try_enable_stage_file_proxy() {

  # Si el módulo no está instalado lo instalamos
  # y lo excluimos en el settings.lando.php de la configuración

  _stage_file_proxy_is_enabled ENABLED
  if [[ "$ENABLED" == "false" ]];  then
    drush en stage_file_proxy

    # aca verificamos el lando.settings.php
    IGNORED_IN_SETTINGS=$(grep -r "settings['config_exclude_modules'] = ['stage_file_proxy'];" /app/web/sites/default/)
    if [[ "$IGNORED_IN_SETTINGS" = "" ]]; then
      echo -e "\n\$settings['config_exclude_modules'] = ['stage_file_proxy'];" >> /app/web/sites/default/settings.lando.php
    fi
    verbose_2_print "Enabled Stage File Proxy"

  fi

  # validamos que esté encendido
  _stage_file_proxy_is_enabled ENABLED
  if [[ "$ENABLED" == "true" ]];  then
    verbose_1_print "Stage File Proxy is enabled."
  else
    print_error "Stage File Proxy is NOT enabled.\nEXIT ON ERROR"
    exit 1
  fi

  # todo validar config seteada e ignorada
}


# Preguntamos si el módulo existe en el filesystem. Para eso
# buscamos el info.yml file
_stage_file_proxy_is_installed() {
  declare -n RESULT=$1
  IS_INSTALLED=$(find $PATH_TO_SITES_FOLDER/modules -name "stage_file_proxy.info.yml")
  if [[ "$IS_INSTALLED" == "" ]];
  then
    RESULT="false"
  else
    RESULT="true"
  fi
}

# Preguntamos si el módulo está instalado en la base. Para eso
# usamos drush eval
_stage_file_proxy_is_enabled() {
  declare -n IS_ENABLED=$1
  IS_ENABLED=$(drush eval 'if(\Drupal::service("module_handler")->moduleExists("stage_file_proxy")==true){return "true";} else {return "false";}')
}
