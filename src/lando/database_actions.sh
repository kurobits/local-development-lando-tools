#!/bin/sh

. "${PATH_LOCAL}/src/variables.sh"


#exit
check_environment() {
  environment=$1
  if [[ $environment ]]; then
    #REMOTE_DATABASE="${REMOTE_DB_PATH}/${environment}_${REMOTE_DB_NAME}.gz"

    verbose_3_print "Looking for database file in remote server"
    FILE_FOUND=$(ssh ${REMOTE_SERVER} "find ${REMOTE_DB_PATH} -name ${environment}_${REMOTE_DB_NAME}.gz")

    if [[ $FILE_FOUND ]]; then
      verbose_1_print "Remote database dump found."
    else
      echo -e "${RED}No database found for the branch ${BOLD}${environment}.${ENDCOLOR}"
    fi
  fi
}

fetch_db_server() {
  verbose_1_print "Copying db from remote"
  verbose_2_print "${REMOTE_SERVER}:${REMOTE_DB_PATH}/${REMOTE_DB_NAME}.gz"
  scp "${REMOTE_SERVER}:${REMOTE_DB_PATH}/${REMOTE_DB_NAME}.gz" /app/local/$DATABASE.gz
}

store_db_backup() {
  eval "drush sql-dump > /app/local/backup_$DATABASE";
  eval "gzip -f /app/local/backup_$DATABASE";
  verbose_1_print "Backup saved at /app/local/backup_$DATABASE.gz";

}

store_db_server() {
  eval "drush sql-dump > /app/local/$DATABASE";
  eval "gzip -f /app/local/$DATABASE";
  eval "scp /app/local/$DATABASE.gz ${REMOTE_SERVER}:${REMOTE_DB_PATH}/${REMOTE_DB_NAME}.gz";
}

drush_import() {
  verbose_1_print "Running updates to the database"
  eval "drush -y updb";
  eval "drush cr";
  verbose_1_print "Importing configuration"
  eval "drush -y cim";
  verbose_1_print "Clearing caches"
  eval "drush -y cr";
  verbose_1_print "Setting admin credentials"
  eval "drush scr /app/local/src/lando/update_admin.php";

}

import_server() {
  if [[ $USE_LOCAL_DB && -f "/app/local/${DATABASE}.gz" ]]; then
    verbose_1_print "Using locally stored database file."
  else
    if [[ $USE_LOCAL_DB ]]; then
      verbose_1_print "Unable to use locally stored db."
    fi
    fetch_db_server "${REMOTE_SERVER}:${REMOTE_DB_PATH}/${REMOTE_DB_NAME}.gz";
    FETCHED=$?
    if [ $FETCHED -ne 0 ]; then
      echo "Unable to fetch database. Skip drop/import."
      return
    fi
  fi

  verbose_2_print "Dropping current database"
  eval "drush sql-drop -y";
  eval "gunzip -f /app/local/$DATABASE.gz";
  verbose_1_print "Importing database file"
  eval "drush sqlc < /app/local/$DATABASE";
  drush_import;

  eval "gzip -f /app/local/$DATABASE";

}
