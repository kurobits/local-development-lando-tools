## Setup: First install

1) Edit your Drupal composer.json file:

  a) On the `"repositories"` key add the following:

```yaml
"repositories": [
    {
        "type": "vcs",
        "name": "easytechgreen/lando",
        "url": "https://bitbucket.org/kurobits/local-development-lando-tools.git"
    }
]
```
  b) On the `"require-dev"` key add the following:

```yaml
"easytechgreen/lando": "^8.0",
```


  d) Under `"extra"` add the following:

```yaml
"extra": {
    ...
    "installer-types": [
        "local-project"
    ],
    "installer-paths": {
        "local": [
            "type:local-project"
        ]
    }
}
```

  e) For the installation to work, check that these dependencies are included:

    - "composer/installers": "^1.9"
    - "oomphinc/composer-installers-extender": "^2.0"

2) Run this command to get the lando local files.
`composer update easytechgreen/lando`

3) If the `local` dir was created succesfully, copy it's `default.local` file next to the `.lando.yml` file:

```bash
cp local/default.local .local

```
Edit the .local file to include the project's configurations.

4) Run `bash local/initialize` to override the existing `.lando.yml` file and then rebuild the lando project with 

```bash
lando rebuild -y
```

5) Ignore the local folder in git for composer-handled projects (if Pantheon runs composer):
  
```yaml
.gitignore

# add this line at the bottom:
/local
```

6) commit the .local, .lando.yml and .gitignore files



Current project status:
- Runs via composer and does not break in Pantheon
- able to get the code from a drupal 8 project via composer.
- .lando.yml file builds correctly
- local scripts are loaded
- sass installed and compiles/watches. 


