#!/bin/bash


. "${PATH_LOCAL}/src/lando/files_actions.sh"
. "${PATH_LOCAL}/src/lando/database_actions.sh"


# Get helper
help_get() {
  echo -e "The CMD get retrieves the database and files from the remote server.\nOptions for the CMD get:\n";
  echo -e "-e, --env\t\t${ITALIC}Allows fetching the database and files from a\n\t\t\tspecific branch (branches must be uploaded\n\t\t\tseparately to the remote server).${ENDCOLOR}\n";
  echo -e "\t\t\t$ lando local get --env=[branch_name]";
  echo -e "\t\t\t$ lando local get -e [branch_name]\n\n";

  echo -e "-c, --component\t\t${ITALIC}Allows fetching a specific component.${ENDCOLOR}\n"
  echo -e "\t\t\t$ lando local get --component=[component_name]";
  echo -e "\t\t\t$ lando local get -c [component_name]\n\n";
  echo -e "\t\t\t${ITALIC}Database:\n\t\t\tTo get the latest database from our custom server\n\t\t\tuse either of these:${ENDCOLOR}"
  echo -e "\t\t\t$ lando local get --component=db"
  echo -e "\t\t\t$ lando local get -c db"
  echo -e "\t\t\t$ lando local get --component=database";
  echo -e "\t\t\t$ lando local get -c database\n\n";
  echo -e "\t\t\t${ITALIC}Files:\n\t\t\tTo get the latest files from our custom server\n\t\t\tuse:${ENDCOLOR}"
  echo -e "\t\t\t$ lando local get --component=files";
  echo -e "\t\t\t$ lando local get -c files\n\n";
  echo -e "\t\t\t${ITALIC}Get everything:\n\t\t\tThis is the option by default. Use either of\n\t\t\tthese:${ENDCOLOR}"
  echo -e "\t\t\t$ lando local get"
  echo -e "\t\t\t$ lando local get --component=all";
  echo -e "\t\t\t$ lando local get -c all\n\n";

  echo -e "--use-local-db\t\t${ITALIC}Loads the database stored locally from the\n\t\t\tprevious import.${ENDCOLOR}\n"

  exit
}


local_get_setup() {
  # parse parameters of CMD get
  array=$1

  arraylength=${#array[@]}
  for (( i=0; i<${arraylength}; i++ ));
  do
    # environment
    if [[ "${array[$i]}" == --env=* ]];
    then
      if [[ $USE_LOCAL_DB && -f "/app/local/${DATABASE}.gz" ]]; then
        print_error "Unable to set environment (--use-local-db is set)."
        exit
      fi
      ENV_SERVER="${array[$i]:6}"
      verbose_1_print "Requested ${ENV_SERVER} environment"
      check_environment $ENV_SERVER
    fi
    if [[ "${array[$i]}" == -e* && "${array[$i+1]}" != -* ]];
    then
      if [[ $USE_LOCAL_DB && -f "/app/local/${DATABASE}.gz" ]]; then
        print_error "Unable to set environment (--use-local-db is set)."
        exit
      fi
      ENV_SERVER="${array[$i+1]}"
      verbose_1_print "Requested ${ENV_SERVER} environment"
      check_environment $ENV_SERVER
    fi
    # component
    if [[ "${array[$i]}" == --component=* ]];
    then
      component="${array[$i]:12}"
      verbose_1_print "Requested $component component"
    fi
    if [[ "${array[$i]}" == -c* && "${array[$i+1]}" != -* ]];
    then
      component="${array[$i+1]}"
      verbose_1_print "Requested $component component"
    fi
    # save db file
    if [[ "${array[$i]}" == --use-local-db* ]];
    then
      USE_LOCAL_DB=true
      verbose_1_print "Requested to use the local database file."
    fi

    if [[ "${array[$i]}" == --help* || "${array[$i]}" == -h* ]];
    then
      help_get
      break
    fi
  done
}


## Get options
local_get() {
  case $1 in
        'database'|'db')
            echo "Getting the latest database!"

            if [[ $ENV_SERVER ]]; then
              REMOTE_DB_NAME="${ENV_SERVER}_${REMOTE_DB_NAME}"
            fi

            import_server
            exit
            ;;
        'files')
            if [[ $USE_LOCAL_DB ]]; then
              verbose_1_print "Not bringing files (--use-local-db is set)."
              exit
            fi
            echo "Getting the latest files!"
            rsync_server
            exit
            ;;
        'all')
            echo "Getting everything!"
            if [[ $ENV_SERVER ]]; then
              REMOTE_DB_NAME="${ENV_SERVER}_${REMOTE_DB_NAME}"
            fi
            import_server

            if [[ $USE_LOCAL_DB ]]; then
              verbose_1_print "Not bringing files (--use-local-db is set)."
              exit
            fi
            rsync_server

            exit
            ;;
        '-h'|'--help')
            help_get
            exit
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)
            echo -e "\n\nUnknown options\n\n"
            help_get
            exit
    esac
}

