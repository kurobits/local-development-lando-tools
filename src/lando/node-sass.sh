#!/bin/bash

SOURCE="sass"
DESTINATION="css"

while (( "$#" )); do
  case "$1" in
    -source|--source|--source=*)
      if [ "${1##--source=}" != "$1" ]; then
        SOURCE="${1##--source=}"
        shift
      else
        SOURCE=$2
        shift 2
      fi
      ;;
    -destination|--destination|--destination=*)
      if [ "${1##--destination=}" != "$1" ]; then
        DESTINATION="${1##--destination=}"
        shift
      else
        DESTINATION=$2
        shift 2
      fi
      ;;
    *)
      shift
      ;;
  esac
done


# echo $(/app/local/node/node_modules/node-sass/bin/node-sass ${SOURCE} -o ${DESTINATION} --output-style expanded --source-map true)
echo $(/app/local/node/node_modules/node-sass/bin/node-sass --importer /app/local/node/node_modules/node-sass-glob-importer/dist/cli.js  ${SOURCE} -o ${DESTINATION} --output-style expanded --source-map true) #-o dist src/index.scss)
# --watch