exit_if_running_from_scripts_folder() {
	if [[ "$BASENAME_DIR" == "src" || "$BASENAME_DIR" == "local" ]]; then
		echo "Run this script from the root folder of your project."
		exit -1
	fi
}

move_previous_lando_file_if_exists() {
	if [[ -f "$LANDO_FILE" ]]; then
		mv $LANDO_FILE $OLD_LANDO_FILE -f
		#echo "debug"
	fi
}

remove_previous_lando_file_if_exists() {
	if [[ -f "$OLD_LANDO_FILE" ]]; then
		rm -f $OLD_LANDO_FILE
		#echo "debug"
	fi
}

