#!/bin/bash
# Run this script from the root folder of your project.
# This script generates the .lando.yml file

# get script location
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

PATH_LOCAL=$DIR
. $DIR/src/bash/functions.sh
. $DIR/src/bash/verbose_and_error.sh

# validate that the script is not running inside the "local" directory
BASENAME_DIR="$(basename $PWD)"
exit_if_running_from_scripts_folder
# BASENAME_DIR="$(basename $PWD)"
# if [[ "$BASENAME_DIR" == "src" || "$BASENAME_DIR" == "local" ]]; then
#   echo "Run this script from the root folder of your project."
#   exit -1
# fi


# load variables
. $DIR/src/variables.sh


verbose_1_print "Init lando file.\n"
#remove previous lando file
LANDO_FILE=$PWD/.lando.yml
OLD_LANDO_FILE=$PWD/.old_lando.yml
move_previous_lando_file_if_exists


echo -e "name: ${PROJECT_NAME}\nrecipe: ${CONFIG[RECIPE]}" >> $LANDO_FILE
LANDO_INIT_RESULT=$?

# exit now if there are any errors
if [[ ! -f "$LANDO_FILE" ]]; then
    print_error "Script was unable to generate the .lando.yml file in $PWD."
    exit 1
fi

if [ $LANDO_INIT_RESULT -ne 0 ]; then
    print_error "Lando CMD returned an error."
    exit 1
fi



CONFIG_DATA="config:\n"
for element in ${!CONFIG[@]}
do
  #echo "key: ${element}"
  #echo "value: ${CONFIG[$element]}"
  case ${element} in
    "PHP")
      CONFIG_DATA="${CONFIG_DATA}  php: ${CONFIG[$element]}\n"
    ;;
    "VIA")
      CONFIG_DATA="${CONFIG_DATA}  via: ${CONFIG[$element]}\n"
    ;;
    "WEBROOT")
      CONFIG_DATA="${CONFIG_DATA}  webroot: ${CONFIG[$element]}\n"
    ;;
    "DATABASE")
      CONFIG_DATA="${CONFIG_DATA}  database: ${CONFIG[$element]}\n"
    ;;
  esac
done

if [[ "${CONFIG[RECIPE]}" == "platformsh" ]];
then

  # PlatformSH recipe uses "app" service, rest use "appserver"
  SERVICE_APP="app"

  ## rompe ijnet no entiendo bien por qu�.
  PARAMS[DOWNGRADE_COMPOSER]=false

  for element in ${!EXTRA_CONFIG[@]}
  do
    #echo "key: ${element}"
    #echo "value: ${EXTRA_CONFIG[$element]}"
    case ${element} in
      "XDEBUG")
        CONFIG_DATA="${CONFIG_DATA}  xdebug: ${EXTRA_CONFIG[$element]}\n"
      ;;
      "ID")
        CONFIG_DATA="${CONFIG_DATA}  id: ${EXTRA_CONFIG[$element]}\n"
      ;;
    esac
  done
else
  SERVICE_APP="appserver"
fi

if [[ "${CONFIG[RECIPE]}" == "pantheon" ]];
then

  for element in ${!EXTRA_CONFIG[@]}
  do
    #echo "key: ${element}"
    #echo "value: ${EXTRA_CONFIG[$element]}"
    case ${element} in
      "FRAMEWORK")
        CONFIG_DATA="${CONFIG_DATA}  framework: ${EXTRA_CONFIG[$element]}\n"
      ;;
      "SITE")
        CONFIG_DATA="${CONFIG_DATA}  site: ${EXTRA_CONFIG[$element]}\n"
      ;;
      "ID")
        CONFIG_DATA="${CONFIG_DATA}  id: ${EXTRA_CONFIG[$element]}\n"
      ;;
    esac
  done

fi

echo -e "${CONFIG_DATA}" >> $LANDO_FILE



declare -A SERVICES_APP_ARRAY
declare -A SERVICES_NODE_ARRAY
declare -A TOOLING_ARRAY

for element in ${!PARAMS[@]}
do
  #echo "key: ${element}"
  #echo "value: ${PARAMS[$element]}"
  case ${element} in
    "LOCAL_SCRIPTS")
      if ${PARAMS[$element]}; then
        # se agregan en tooling
        TOOLING_ARRAY[LOCAL]=${PARAMS[$element]}
      fi
    ;;

    "VENDOR_DRUSH")
      if ${PARAMS[$element]}; then
        # se agrega en tooling
        TOOLING_ARRAY[DRUSH]=${PARAMS[$element]}
      fi
    ;;

    "COMPASS")
      if ${PARAMS[$element]}; then
        # se agrega en tooling
        TOOLING_ARRAY[COMPASS]=${PARAMS[$element]}
        # se agrega en services
        SERVICES_APP_ARRAY[COMPASS]=${PARAMS[$element]}
      fi
    ;;

    "COMPOSER_PHP_LIMIT")
      if ${PARAMS[$element]}; then
        # se agrega en tooling
        TOOLING_ARRAY[COMPOSER]=${PARAMS[$element]}
      fi
    ;;

    "NODE_SASS")
      if ${PARAMS[$element]}; then
        # se agrega en tooling
        TOOLING_ARRAY[NODE_SASS]=${PARAMS[$element]}
        # se agrega en services
        SERVICES_NODE_ARRAY[NODE_SASS]=${PARAMS[$element]}
      fi
    ;;

    "SASS")
      if ${PARAMS[$element]}; then
        # se agrega en tooling
        TOOLING_ARRAY[SASS]=${PARAMS[$element]}
        # se agrega en services
        SERVICES_NODE_ARRAY[SASS]=${PARAMS[$element]}
      fi
    ;;

    "NODE_LESS")
      if ${PARAMS[$element]}; then
        echo "node less?"
      fi
    ;;

    "DOWNGRADE_COMPOSER")
      if ${PARAMS[$element]}; then
        # se agrega en services
        SERVICES_APP_ARRAY[COMPOSER]=${PARAMS[$element]}
      fi
    ;;

    *)
      echo "default"
      ;;
  esac
done

if [ ! ${#SERVICES_APP_ARRAY[@]} -eq 0 ] ||  [ ! ${#SERVICES_NODE_ARRAY[@]} -eq 0 ] ; then
  echo -e "services:" >> $LANDO_FILE

  if [ ! ${#SERVICES_APP_ARRAY[@]} -eq 0 ]; then
    #echo -e "\nAppserver\n"



    APPSERVER="  ${SERVICE_APP}:"
    for element in ${!SERVICES_APP_ARRAY[@]}
    do
      case ${element} in
        "COMPOSER")
            APPSERVER="${APPSERVER}\n    type: php\n    composer_version: '1.10.1'"
          ;;
        *)
          echo "default"
        ;;
      esac

    done
    echo -e "${APPSERVER}" >> $LANDO_FILE

  fi

  if [ ! ${#SERVICES_NODE_ARRAY[@]} -eq 0 ] ; then
    #echo -e "\nNode\n"
    NODE_APP="  node:"
    for element in ${!SERVICES_NODE_ARRAY[@]}
    do
      case ${element} in
        "NODE_SASS")
          NODE_APP="${NODE_APP}\n    type: 'node:10'\n    run:\n      - 'mkdir -p /app/local/node && cd /app/local/node && npm install babel-core babel-preset-env bourbon bourbon-neat browser-sync gulp gulp-autoprefixer gulp-babel gulp-imagemin gulp-pixrem gulp-sass gulp-sass-glob gulp-sourcemaps gulp-svgmin gulp-uglify gulp-watch node-sass node-sass-glob-importer yargs'"
          ;;
        "SASS")
          NODE_APP="${NODE_APP}\n    type: 'node:14'\n    run:\n      - 'mkdir -p /app/local/node && cd /app/local/node && npm install sass bourbon bourbon-neat'"
          ;;
        *)
          echo "default"
          ;;

      esac
    done

    echo -e "${NODE_APP}" >> $LANDO_FILE
  fi

fi


if [ ! ${#TOOLING_ARRAY[@]} -eq 0 ]; then
  #echo -e "\nTooling\n"
  TOOLING=""
  for element in ${!TOOLING_ARRAY[@]}
  do
    #echo "key: ${element}"
    #echo "value: ${TOOLING_ARRAY[$element]}"

    case ${element} in

      "LOCAL")
        TOOLING="$TOOLING  local:\n    service: $SERVICE_APP\n    description: Custom local scripts. Use -h to get more information.\n    cmd: /app/local/src/lando/actions.sh\n    level: app\n"
        ;;

      "DRUSH")
        TOOLING="$TOOLING  drush:\n    service: $SERVICE_APP\n    cmd: /app/vendor/bin/drush --root=/app/web\n"
        ;;

      "COMPASS")
        echo "compass esta deprecado"
        ;;

      "COMPOSER")
        TOOLING="$TOOLING  composer:\n    service: $SERVICE_APP\n    cmd: php -dmemory_limit=-1 /usr/bin/composer\n"
        ;;

      "SASS")
        TOOLING="$TOOLING  sass:\n    service: node\n"
        ;;

      *)
        echo "default"
        ;;
    esac
  done
  echo -e "tooling:" >> $LANDO_FILE
  echo -e "${TOOLING}" >> $LANDO_FILE
fi


DIFF=$(diff $LANDO_FILE $OLD_LANDO_FILE)
if [[ $DIFF ]]; then
  echo -e "Project is set up. Please run\n\n  lando rebuild -y\n"
fi

remove_previous_lando_file_if_exists